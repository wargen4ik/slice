<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                    'attribute' => 'fio',
                'value' => function ($model) {
                    return $model->user->email;
                }
            ],
            [
                    'attribute' => 'goods_name',
                'value' => function ($model) {
                    return $model->goods->name;
                }
            ],
            'text:ntext',
            'rating',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
