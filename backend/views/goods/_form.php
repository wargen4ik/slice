<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Goods */
/* @var $logoModel common\models\Image */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="goods-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->input('number') ?>

    <?= $form->field($model, 'description')->widget(\mihaildev\ckeditor\CKEditor::className(), [
        'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions('elfinder', [
            'preset' => 'full',
            'inline' => false,
        ])
    ]); ?>

    <?= $form->field($model, 'is_available')->checkbox() ?>

    <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(\common\models\Type::find()->all(),'id','name'), [
        'prompt' => Yii::t('app', 'Select type...')
    ]) ?>

    <?= $form->field($model, 'brand_id')->dropDownList(ArrayHelper::map(\common\models\Brand::find()->all(),'id','name'), [
        'prompt' => Yii::t('app', 'Select brand...')
    ]) ?>

    <?= $form->field($logoModel, 'imageFile')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
