<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\GoodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Goods';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goods-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Goods', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'price',
//            [
//                    'attribute' => 'description',
//                'format' => 'raw'
//            ],
            [
                    'attribute' => 'is_available',
                'value' => function ($model){
        return $model->is_available ? Yii::t('app', 'Available') : Yii::t('app', 'Not available');
                },
                'filter' => [
                        '0' => Yii::t('app', 'Available'),
                    '1' => Yii::t('app', 'Not available')
                ]
            ],
            [
                    'attribute' => 'type_id',
                'value' => function ($model){
                    return $model->type->name;
                },
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Type::find()->all(), 'id', 'name')
            ],
            [
                'attribute' => 'brand_id',
                'value' => function ($model){
                    return $model->brand->name;
                },
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Brand::find()->all(), 'id', 'name')
            ],
            [
                    'attribute' => 'created_at',
                'value' => function($model){
                    return Yii::$app->formatter->asDate($model->created_at);
                },
                'filter' => \janisto\timepicker\TimePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'mode' => 'date',
                    'clientOptions' => [
                        'dateformat' => Yii::$app->formatter->dateFormat,
                    ]
                ]),
            ],
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
