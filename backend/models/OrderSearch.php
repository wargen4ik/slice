<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form of `common\models\Order`.
 */
class OrderSearch extends Order
{
    public $username;
    public $delivery_type;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'created_at', 'delivery_type',], 'integer'],
            [['username',], 'string'],
            [['session_id', 'username', 'delivery_type',], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith('delivery')
            ->joinWith('user')
            ->leftJoin('order_bucket_item', 'order_bucket_item.id=order.id')
            ->leftJoin('bucket_item', 'order_bucket_item.bucket_item_id=bucket_item.id')
            ->leftJoin('goods', 'bucket_item.goods_id=goods.id');

        // grid filtering conditions
        $query->andFilterWhere([
            'order.id' => $this->id,
            'order.user_id' => $this->user_id,
            'order.created_at' => $this->created_at,
        ]);

//        $query->andFilterWhere([
//            'delivery.delivery_type' => $this->delivery_type,
//        ]);

        $query->andFilterWhere(['like', 'delivery.fio', $this->username]);
        $query->andFilterWhere(['=', 'delivery.delivery_type', $this->delivery_type]);
//        $query->andFilterWhere(['like', 'goods.name', $this->goods_name]);

        return $dataProvider;
    }
}
