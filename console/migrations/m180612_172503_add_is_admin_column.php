<?php

use yii\db\Migration;

/**
 * Class m180612_172503_add_is_admin_column
 */
class m180612_172503_add_is_admin_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'is_admin', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'is_admin');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180612_172503_add_is_admin_column cannot be reverted.\n";

        return false;
    }
    */
}
