<?php

use yii\db\Migration;

/**
 * Handles the creation of table `delivery`.
 */
class m180524_161123_create_delivery_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('delivery', [
            'id' => $this->primaryKey(),
            'phone' => $this->string(255)->notNull(),
            'address' => $this->string(255)->notNull(),
            'delivery_type' => $this->integer(2)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('delivery');
    }
}
