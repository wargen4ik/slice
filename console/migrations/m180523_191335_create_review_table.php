<?php

use yii\db\Migration;

/**
 * Handles the creation of table `review`.
 */
class m180523_191335_create_review_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('review', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
            'goods_id' => $this->integer(11)->notNull(),
            'text' => $this->text(),
            'rating' => $this->smallInteger(2)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('review');
    }
}
