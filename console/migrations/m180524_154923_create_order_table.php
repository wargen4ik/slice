<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m180524_154923_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11),
            'session_id' => $this->string(255),
            'delivery_id' => $this->integer(11)->notNull(),
            'goods_id' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'count' => $this->integer(5)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order');
    }
}
