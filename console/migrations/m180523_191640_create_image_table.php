<?php

use yii\db\Migration;

/**
 * Handles the creation of table `image`.
 */
class m180523_191640_create_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('image', [
            'id' => $this->primaryKey(),
            'goods_id' => $this->integer(11)->notNull(),
            'is_logo' => $this->boolean()->defaultValue(false),
            'path' => $this->string(511),
            'name' => $this->string(255)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('image');
    }
}
