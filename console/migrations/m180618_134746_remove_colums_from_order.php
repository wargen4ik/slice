<?php

use yii\db\Migration;

/**
 * Class m180618_134746_remove_colums_from_order
 */
class m180618_134746_remove_colums_from_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('order', 'count');
        $this->dropColumn('order', 'goods_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180618_134746_remove_colums_from_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180618_134746_remove_colums_from_order cannot be reverted.\n";

        return false;
    }
    */
}
