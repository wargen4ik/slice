<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contact`.
 */
class m180523_191342_create_contact_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contact', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255)->notNull(),
            'text' => $this->text()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('contact');
    }
}
