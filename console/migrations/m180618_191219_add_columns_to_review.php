<?php

use yii\db\Migration;

/**
 * Class m180618_191219_add_columns_to_review
 */
class m180618_191219_add_columns_to_review extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('review', 'created_at', $this->integer(11)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180618_191219_add_columns_to_review cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180618_191219_add_columns_to_review cannot be reverted.\n";

        return false;
    }
    */
}
