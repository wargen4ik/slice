<?php

use yii\db\Migration;

/**
 * Class m180618_134923_add_columns_to_delivery
 */
class m180618_134923_add_columns_to_delivery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('delivery', 'fio', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180618_134923_add_columns_to_delivery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180618_134923_add_columns_to_delivery cannot be reverted.\n";

        return false;
    }
    */
}
