<?php

use yii\db\Migration;

/**
 * Handles the creation of table `goods`.
 */
class m180523_185533_create_goods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('goods', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'price' => $this->integer(11)->notNull(),
            'description' => $this->text(),
            'is_available' => $this->boolean()->defaultValue(true),
            'type_id' => $this->integer(11)->notNull(),
            'brand_id' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('goods');
    }
}
