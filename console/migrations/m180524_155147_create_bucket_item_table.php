<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bucket_item`.
 */
class m180524_155147_create_bucket_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bucket_item', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11),
            'session_id' => $this->string(255),
            'goods_id' => $this->integer(11)->notNull(),
            'count' => $this->integer(5)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('bucket_item');
    }
}
