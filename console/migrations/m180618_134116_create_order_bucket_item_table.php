<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_bucket_item`.
 */
class m180618_134116_create_order_bucket_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_bucket_item', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(11),
            'bucket_item_id' => $this->integer(11),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order_bucket_item');
    }
}
