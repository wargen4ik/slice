<?php

use yii\db\Migration;

/**
 * Class m180618_145418_add_columns_to_bucket_item
 */
class m180618_145418_add_columns_to_bucket_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bucket_item', 'is_active', $this->boolean()->defaultValue(true));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180618_145418_add_columns_to_bucket_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180618_145418_add_columns_to_bucket_item cannot be reverted.\n";

        return false;
    }
    */
}
