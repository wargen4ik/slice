<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 04.06.18
 * Time: 22:56
 */

namespace frontend\controllers;


use common\models\BucketItem;
use common\models\Delivery;
use common\models\Goods;
use common\models\Order;
use common\models\OrderBucketItem;
use Yii;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BucketController extends Controller
{
    public function actionAdd($id, $return_url = '/', $count = 1, $total_count = null){
        $cookies = \Yii::$app->request->cookies;
        $bucketItem = null;

        if (\Yii::$app->user->isGuest){
            if (!$cookies->getValue('session_id', false)){
                \Yii::$app->response->cookies->add(new Cookie([
                    'name' => 'session_id',
                    'value' => \Yii::$app->session->id
                ]));
            }
            if (false != ($bucketItem = BucketItem::findOne(['goods_id' => $id, 'session_id' => \Yii::$app->request->cookies->get('session_id'), 'is_active' => true]))){
                if (!$total_count) {
                    $bucketItem->count += $count;
                    $bucketItem->save();
                } else {
                    $bucketItem->count = $total_count;
                    $bucketItem->save();
                }
            } else {
                $bucketItem = new BucketItem(['session_id' => \Yii::$app->request->cookies->getValue('session_id'), 'goods_id' => $id]);
                if (!$total_count) {
                    $bucketItem->count += $count;
                    $bucketItem->save();
                } else {
                    $bucketItem->count = $total_count;
                    $bucketItem->save();
                }
            }
        } else {
            if (false != ($bucketItem = BucketItem::findOne(['goods_id' => $id, 'user_id' => \Yii::$app->user->id, 'is_active' => true]))){
                if (!$total_count) {
                    $bucketItem->count += $count;
                    $bucketItem->save();
                } else {
                    $bucketItem->count = $total_count;
                    $bucketItem->save();
                }
            } else {
                $bucketItem = new BucketItem(['user_id' => \Yii::$app->user->id, 'goods_id' => $id]);
                if (!$total_count) {
                    $bucketItem->count += $count;
                    $bucketItem->save();
                } else {
                    $bucketItem->count = $total_count;
                    $bucketItem->save();
                }
            }
        }
        if (\Yii::$app->request->isAjax && $bucketItem){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'success' => empty($bucketItem->errors),
                'total_price' => $bucketItem->count * $bucketItem->goods->price,
                'price' => $bucketItem->goods->price,
            ];
        }

        return $this->redirect($return_url);
    }

    public function actionRemove($id, $return_url = '/'){
        $model = $this->findModel($id);
        $goods = $model->goods;
        $total_price = $model->count * $goods->price;

        if (\Yii::$app->user->isGuest){
            if ($model->session_id === \Yii::$app->request->cookies->getValue('session_id', '')){
                $model->delete();
            }
        } else {
            if ($model->user_id === \Yii::$app->user->id){
                $model->delete();
            }
        }

        if (\Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'success' => empty($model->errors),
                'total_price' => $total_price,
            ];
        }
        return $this->redirect($return_url);
    }

    public function actionIndex(){
        $delivery = null;
        $bucket_items = [];

        if (\Yii::$app->user->isGuest){
            $bucket_items = BucketItem::findAll(['session_id' => \Yii::$app->request->cookies->getValue('session_id', ''), 'is_active' => true]);
            $delivery = Order::findOne(['session_id' => \Yii::$app->request->cookies->getValue('session_id', '')]);
        } else {
            $bucket_items = BucketItem::findAll(['user_id' => \Yii::$app->user->id, 'is_active' => true]);
            $delivery = Order::findOne(['user_id' => Yii::$app->user->id]);
        }

        if (!$delivery){
            $delivery = new Delivery();
        } else {
            $delivery = $delivery->delivery;
        }

        if ($bucket_items && $delivery->load(Yii::$app->request->post()) && $delivery->save()){
            $order = new Order();
            $order->delivery_id = $delivery->id;
            if (Yii::$app->user->isGuest){
                $order->session_id = \Yii::$app->request->cookies->getValue('session_id', '');
            } else {
                $order->user_id = Yii::$app->user->id;
            }
            $order->created_at = time();
            $order->save();
            foreach ($bucket_items as $bucket_item){
                $bucket_item->is_active = false;
                $bucket_item->save(false);
                (new OrderBucketItem(['bucket_item_id' => $bucket_item->id, 'order_id' => $order->id]))->save(false);
            }
            return $this->redirect('/');
        }

        return $this->render('index', [
            'bucket_items' => $bucket_items,
            'delivery' => $delivery
        ]);
    }

    protected function findGoods($id){
        if (($model = Goods::findOne($id)) != false){
            return $model;
        }
        throw new NotFoundHttpException(\Yii::t('app', 'Requested item was not found.'));
    }

    protected function findModel($id){
        if (($model = BucketItem::findOne(['id' => $id, 'is_active' => true])) != false){
            return $model;
        }
        throw new NotFoundHttpException(\Yii::t('app', 'Requested item was not found.'));
    }
}