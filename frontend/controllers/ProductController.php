<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 07.06.18
 * Time: 21:30
 */

namespace frontend\controllers;


use common\models\Brand;
use common\models\Goods;
use common\models\Review;
use common\models\Type;
use frontend\models\search\GoodsSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ProductController extends Controller
{
    public function actionIndex($id){
        $goods = Goods::findOne($id);
        if (!$goods){
            throw new NotFoundHttpException();
        }
        $reviews = Review::find()->where(['goods_id' => $goods->id])->orderBy('created_at DESC')->all();
        $can_add_review = false;
        if (!Yii::$app->user->isGuest) {
            $can_add_review = Review::findOne(['goods_id' => $goods->id, 'user_id' => Yii::$app->user->id]) ? false : true;
        }

        return $this->render('index', [
            'goods' => $goods,
            'reviews' => $reviews,
            'can_add_review' => $can_add_review
        ]);
    }

    public function actionShop(){
        $searchModel = new GoodsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $types = Type::findBySql('Select `type`.`name`, count(`goods`.`id`) as `count` from `type` left join `goods` on `type`.`id` = `goods`.`type_id` GROUP by `type`.`name` Order by `count` desc')->all();

        $brands = Brand::findBySql('Select `brand`.`name`, count(goods.id) as `count` from `brand` left join `goods` on `goods`.`brand_id` = `brand`.`id` group by `brand`.`name` ORDER BY `count` desc')->all();

        return $this->render('shop', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'types' => $types,
            'brands' => $brands,
        ]);
    }

    public function actionAddReview($return_url = '/'){
        $review = new Review();

        if ($review->load(Yii::$app->request->post(), '') && $review->save()){
            return $this->redirect($return_url);
        }
        return $this->redirect($return_url);
    }
}