<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 24.05.18
 * Time: 22:40
 */

namespace frontend\controllers;


use common\models\Image;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ImageController extends Controller
{
    public function actionIndex($name)
    {
        $image = Image::findOne(['name' => $name]);

        if (!$image) {
            throw new NotFoundHttpException('The requested image does not exist.');
        }

        $response = Yii::$app->getResponse();
        $response->headers->set('Content-Type', 'image/jpeg');
        $response->format = Response::FORMAT_RAW;
        if ( !is_resource($response->stream = fopen($image->getFullPath(), 'r')) ) {
            throw new \yii\web\ServerErrorHttpException('file access failed: permission deny');
        }
        return $response->send();
    }
}