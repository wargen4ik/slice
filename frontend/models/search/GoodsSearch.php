<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Goods;
use yii\db\Expression;

/**
 * GoodsSearch represents the model behind the search form of `common\models\Goods`.
 */
class GoodsSearch extends Goods
{
    public $type;
    public $brand;
    public $priceFrom;
    public $priceTo;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'price', 'is_available', 'type_id', 'brand_id', 'created_at', 'updated_at', 'priceFrom', 'priceTo'], 'integer'],
            [['name', 'description', 'brand', 'type', 'priceFrom', 'priceTo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Goods::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->joinWith(['type', 'brand']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_available' => true,
        ]);
        $query->andFilterWhere(['=', 'type.name', $this->type])
            ->andFilterWhere(['=', 'brand.name', $this->brand]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        if ($this->priceFrom){
            $query->andFilterWhere(['>', 'price', $this->priceFrom]);
        }
        if ($this->priceTo){
            $query->andFilterWhere(['<', 'price', $this->priceTo]);
        }

        return $dataProvider;
    }

    public static function findFilter($filter){
        $query = Goods::find();

        switch ($filter){
            case 'all': $query->orderBy(new Expression('rand()')); break;
            case 'latest': $query->orderBy('created_at DESC'); break;
            case 'bestsellers': break;
            default: break;
        }

        return $query->limit(6)->all();
    }
}
