<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 *
 * @property User $user
 */
class SigninForm extends Model
{
    public $email;
    public $password;

    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', function($attribute){
                $this->_user = User::findByEmail($this->email);
                if (!$this->_user){
                    $this->addError('email', 'Wrong email.');
                }
            }],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password', function($attribute){

                if (!$this->_user || !$this->_user->validatePassword($this->password)){
                    $this->addError('password', \Yii::t('app', 'Wrong password.'));
                }
            }]
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signin()
    {
        if (!$this->validate()) {
            return null;
        }
        \Yii::$app->user->login($this->_user,3600 * 24 * 30);
        return $this->_user;
    }

    public function getUser(){
        return $this->_user;
    }
}
