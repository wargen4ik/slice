<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/font-awesome.min.css',
        'css/prettyPhoto.css',
        'css/jquery.bxslider.css',
        'css/owl.carousel.css',
        'css/responsive.css',
        '//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic%7CPT+Gudea:400,700,400italic%7CPT+Oswald:400,700,300',
        'css/style.css',
        'css/site.css',
    ];
    public $js = [
        'js/smoothscroll.js',
        'js/jquery.debouncedresize.js',
        'js/retina.min.js',
//        'js/jquery.placeholder.js',
//        'js/jquery.hoverIntent.min.js',
//        'js/jquery.flexslider-min.js',
        'js/owl.carousel.min.js',
        'js/jflickrfeed.min.js',
        'js/jquery.prettyPhoto.js',
//        'js/mousewheel.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
