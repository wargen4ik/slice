<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 24.05.18
 * Time: 19:44
 */

use yii\helpers\Html;

$language = Yii::$app->language;
$language_full = [
    'en-US' => 'English',
    'ua' => 'Українська'
];

$languages = [
    'ua',
    'en-US'
];

if (Yii::$app->user->isGuest){
    $bucketItems = \common\models\BucketItem::findAll(['session_id' => Yii::$app->request->cookies->getValue('session_id', ''), 'is_active' => true]);
} else {
    $bucketItems = \common\models\BucketItem::findAll(['user_id' => Yii::$app->user->id, 'is_active' => true]);
}

$sum = 0;

foreach ($bucketItems as $bucketItem){
    $sum += $bucketItem->count * $bucketItem->goods->price;
}

$signinModel = new \frontend\models\SigninForm();
$signupModel = new \frontend\models\SignupForm();

$this->registerJs(<<<JS
    $('#login').click(function() {
        $('#login-modal').modal('show');
    });
    
    $('#signup').click(function() {
        $('#signup-modal').modal('show');
    })
JS
)
?>
<header id="header" class="header3">
    <div id="header-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                        <div class="header-top-left">
                            <ul id="top-links" class="clearfix">
                                <li><a href="/bucket" title="My Cart"><span
                                                class="top-icon top-icon-cart"></span><span
                                                class="hide-for-xs"><?= Yii::t('app', 'My Cart') ?></span></a></li>
                            </ul>
                        </div>
                    <?php if (!Yii::$app->user->isGuest): ?>
                        <div class="header-top-right">
                            <div class="header-text-container pull-right">
                                <p class="header-link"><?= Yii::t('app', 'Welcome, ') . Yii::$app->user->identity->username ?>
                                <a href="/logout"><?= Yii::t('app', 'Logout') ?></a>
                                </p>

                            </div>
                        </div>
                    <?php else: ?>
                        <div class="header-top-right">
                            <div class="header-text-container pull-right">
                                <p class="header-text"><?= Yii::t('app', 'Welcome to Venedor!') ?></p>
                                <p class="header-link"><a id="login"
                                                          href="#login-modal"><?= Yii::t('app', 'login') ?></a>&nbsp;<?= Yii::t('app', 'or') ?>
                                    &nbsp;<a id="signup"
                                             href="#"><?= Yii::t('app', 'create an account') ?></a></p>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>

    <div id="inner-header">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-5 col-xs-12 logo-container">
                    <h1 class="logo clearfix">
                        <span>Slice</span>
                        <a href="/" title="Slice"><img src="/images/logo.png" alt="Slice" width="146"
                                                       height="62"></a>
                    </h1>
                </div>
                <div class="col-md-7 col-sm-7 col-xs-12 header-inner-right">

                    <div class="header-inner-right-wrapper clearfix">
                        <div class="dropdown-cart-menu-container pull-right">
                            <div class="btn-group dropdown-cart">
                                <button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown">
                                    <span class="cart-menu-icon"></span>
                                    <?= count($bucketItems) ?> <?= Yii::t('app', 'item(s)') ?> <span class="drop-price">- ₴<?= $sum ?></span>
                                </button>

                                <div class="dropdown-menu dropdown-cart-menu pull-right clearfix" role="menu">
                                    <p class="dropdown-cart-description"><?= Yii::t('app', 'Recently added item(s).') ?></p>
                                    <ul class="dropdown-cart-product-list">
                                        <?php foreach ($bucketItems as $bucketItem): ?>
                                        <li class="item clearfix">
                                            <a href="/bucket/remove?id=<?= $bucketItem->id ?>&return_url=<?= Yii::$app->request->url ?>" title="<?= Yii::t('app', 'Delete item') ?>"
                                               class="delete-item"><i
                                                        class="fa fa-times"></i></a>
                                            <figure>
                                                <a href="/product/<?= $bucketItem->goods->id ?>"><img
                                                            src="<?= $bucketItem->goods->logo->url ?>"
                                                            alt="$bucketItem->goods->name"></a>
                                            </figure>
                                            <div class="dropdown-cart-details">
                                                <p class="item-name">
                                                    <a href="/product/<?= $bucketItem->goods->id ?>"><?= $bucketItem->goods->name ?> </a>
                                                </p>
                                                <p>
                                                    <?= $bucketItem->count ?>x
                                                    <span class="item-price">₴<?= $bucketItem->goods->price ?></span>
                                                </p>
                                            </div>
                                        </li>
                                        <?php endforeach; ?>
                                    </ul>

                                    <ul class="dropdown-cart-total">
                                        <li><span class="dropdown-cart-total-title"><?= Yii::t('app', 'Total') ?>:</span>₴<?= $sum ?></li>
                                    </ul>
                                    <div class="dropdown-cart-action">
                                        <p><a href="/bucket" class="btn btn-custom-2 btn-block"><?= Yii::t('app', 'Cart') ?></a></p>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="header-top-dropdowns pull-right">
                            <div class="btn-group dropdown-money">
                                <button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown">
                                    <span class="hide-for-xs"><?= Yii::t('app', 'UA Grivna') ?></span><span
                                            class="hide-for-lg">₴</span>
                                </button>
                            </div>
                            <div class="btn-group dropdown-language">
                                <button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown">
                                        <span class="flag-container"><img
                                                    src="/images/<?= $language ?>-flag.png"></span>
                                    <span class="hide-for-xs"><?= $language_full[$language] ?></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <?php foreach ($languages as $lang): ?>
                                        <?php if ($lang != $language): ?>
                                            <li><a href="#"><span class="flag-container"><img
                                                                src="/images/<?= $lang ?>-flag.png"></span><span
                                                            class="hide-for-xs"><?= $language_full[$lang] ?></span></a>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <p class="quick-contact-text"><?= Yii::t('app', 'SUPPORT SERVICE') ?>: +(404) 851-21-48-15;
                        +(404) 158-14-25-78</p>
                </div>
            </div>
        </div>

        <div id="main-nav-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 clearfix">

                        <nav id="main-nav">
                            <div id="responsive-nav">
                                <div id="responsive-nav-button">
                                    Menu <span id="responsive-nav-button-icon"></span>
                                </div>
                            </div>
                            <ul class="menu clearfix">
                                <li>
                                    <a class="active" href="/">
                                        <span class="hide-for-xs"><img src="/images/home-icon.png"
                                                                       alt="home icon"></span>
                                        <span class="hide-for-lg"><?= Yii::t('app', 'Home') ?></span>
                                    </a>
                                </li>
                                <li class="mega-menu-container"><a href="/product/shop"><?= Yii::t('app', 'SHOP') ?></a></li>

                                <li><a href="/site/contact"><?= Yii::t('app', 'Contact Us') ?></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>


<?php \yii\bootstrap\Modal::begin([
    'id' => 'login-modal',
    'header' => '<h2>' . Yii::t('app', 'Login') . '</h2>'
]) ?>

<?php $signinForm = \yii\widgets\ActiveForm::begin([
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'action' => '/login'
]); ?>

<?= $signinForm->field($signinModel, 'email') ?>

<?= $signinForm->field($signinModel, 'password')->passwordInput() ?>

<?= Html::submitButton(\Yii::t('app', 'Login'), ['class' => 'btn btn-form btn-primary']); ?>

<?php \yii\widgets\ActiveForm::end(); ?>
<div class="row" style="margin-top: 10px">
    <div class="col-xs-12">
        <button type="submit" class="btn btn-default btn-default pull-left" data-dismiss="modal"><span
                    class="glyphicon glyphicon-remove"></span> Cancel
        </button>
    </div>
</div>
<?php \yii\bootstrap\Modal::end() ?>

<?php \yii\bootstrap\Modal::begin([
    'id' => 'signup-modal',
    'header' => '<h2>' . Yii::t('app', 'Sign up') . '</h2>'
]) ?>

<?php $signupForm = \yii\widgets\ActiveForm::begin([
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'action' => '/signup'
]); ?>

<?= $signupForm->field($signupModel, 'email') ?>

<?= $signupForm->field($signupModel, 'phone') ?>

<?= $signupForm->field($signupModel, 'password')->passwordInput() ?>
<?= $signupForm->field($signupModel, 'passwordRepeat')->passwordInput() ?>

<?= Html::submitButton(\Yii::t('app', 'Sign up'), ['class' => 'btn btn-form btn-primary']); ?>

<?php \yii\widgets\ActiveForm::end(); ?>
<div class="row" style="margin-top: 10px">
    <div class="col-xs-12">
        <button type="submit" class="btn btn-default btn-default pull-left" data-dismiss="modal"><span
                    class="glyphicon glyphicon-remove"></span> Cancel
        </button>
    </div>
</div>
<?php \yii\bootstrap\Modal::end() ?>
