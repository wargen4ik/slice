<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 24.05.18
 * Time: 19:44
 */

?>
<footer id="footer">
    <div id="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 footer-text-container">
                    <p>&copy; 2018 Slice. Yaroshenko Bogdan.</p>
                </div>
            </div>
        </div>
    </div>

</footer>
