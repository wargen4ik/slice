<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 07.06.18
 * Time: 21:33
 */

use yii\data\Sort;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;

/* @var $goods \common\models\Goods[] */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\models\search\GoodsSearch */

$sort = new Sort([
    'sortParam' => 'sort',
    'attributes' => [
        'created_at' => [
            'asc' => SORT_ASC,
            'desc' => SORT_DESC,
            'label' => Yii::t('app', 'By date')
        ],
        'name' => [
            'asc' => SORT_ASC,
            'desc' => SORT_DESC,
            'label' => Yii::t('app', 'By name')
        ],
        'price' => [
                'asc' => SORT_ASC,
            'desc' => SORT_DESC,
            'label' => Yii::t('app', 'By price')
        ]
    ],
]);

$pages = new \yii\data\Pagination(['totalCount' => $dataProvider->count, 'pageSize' => Yii::$app->request->get('per-page', 10)]);
$request = Yii::$app->request;
$curr_type = ArrayHelper::getValue($request->get(), 'GoodsSearch.type');
$curr_brand = ArrayHelper::getValue($request->get(), 'GoodsSearch.brand');
$this->registerJs(<<<JS
    
    $('#limit-dropdown>li').click(function(e) {
      $('input[name="per-page"]').val($(e.target).text());
     $('#filter-form').submit();
    });

    $('#type-ul>li').click(function(e) {
      $('input[name="GoodsSearch[type]"]').val($(e.target).attr('id'));
     $('#filter-form').submit();
    });
    
    $('#brand-ul>li').click(function(e) {
      $('input[name="GoodsSearch[brand]"]').val($(e.target).attr('id'));
     $('#filter-form').submit();
    });

    $('#price-clear').click(function() {
      $('#price-range-low').val('');
      $('#price-range-high').val('');
      $('#filter-form').submit();
    });
    
    $('.glyphicon').mouseenter(function() {
        $(this).removeClass('glyphicon-ok');
        $(this).addClass('glyphicon-remove');
    });
    
    $('.glyphicon').mouseleave(function() {
        console.log('1');
        $(this).removeClass('glyphicon-remove');
        $(this).addClass('glyphicon-ok');
    });
   
JS
);
$this->registerCss(<<<CSS
    .glyphicon-remove:hover {
        color: darkred;
        cursor: pointer;
    }
CSS
);
?>

<div class="row">
    <div class="col-xs-11">
        <div id="category-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/"><?= Yii::t('app', 'Home') ?></a></li>
                    <li class="active"><?= Yii::t('app', 'Shop') ?></li>
                </ul>
            </div>
        </div>
        <?php \yii\widgets\ActiveForm::begin(['method' => 'get', 'id' => 'filter-form', 'action' => '/product/shop']) ?>
        <div class="row">
            <div class="col-md-12">

                <div class="row">

                    <div class="col-md-9 col-sm-8 col-xs-12 main-content">

                        <div class="category-toolbar clearfix">
                            <div class="toolbox-filter clearfix" style="z-index: 1">

                                <div class="sort-box">
                                    <span class="separator"><?= Yii::t('app', 'sort by') ?>:</span>
                                    <div class="btn-group select-dropdown">
                                        <button type="button" class="btn select-btn"><?= Yii::t('app', 'Sort') ?></button>
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><?= $sort->link('created_at') ?></li>
                                            <li><?= $sort->link('name') ?></li>
                                            <li><?= $sort->link('price') ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- End .toolbox-filter -->
                            <div class="toolbox-pagination clearfix">
                                <?= \yii\widgets\LinkPager::widget([
                                    'pagination' => $pages
                                ]) ?>
                                <div class="view-count-box">
                                    <span class="separator"><?= Yii::t('app', 'count per page') ?>:</span>
                                    <div class="btn-group select-dropdown">
                                        <button type="button"
                                                class="btn select-btn"><?= Yii::$app->request->get('per-page', 10) ?></button>
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <?= \yii\helpers\Html::hiddenInput('per-page', Yii::$app->request->get('per-page', 10)) ?>
                                        <ul class="dropdown-menu" role="menu" id="limit-dropdown">
                                            <li><a href="#">10</a></li>
                                            <li><a href="#">15</a></li>
                                            <li><a href="#">30</a></li>
                                        </ul>
                                    </div>
                                </div><!-- End .view-count-box -->

                            </div><!-- End .toolbox-pagination -->


                        </div><!-- End .category-toolbar -->
                        <div class="md-margin"></div><!-- .space -->
                        <div class="category-item-container">
                            <div class="row">
                                <?= ListView::widget([
                                    'dataProvider' => $dataProvider,
                                    'layout' => '{items}<div class="col-xs-12">{summary}</div>',
                                    'itemView' => '_item',
                                    'viewParams' => [
                                        'dataProvider' => $dataProvider,
                                    ]
                                ]) ?>
                            </div><!-- End .row -->
                        </div><!-- End .category-item-container -->

                        <div class="pagination-container clearfix">
                            <div class="pull-right">
                                <?= \yii\widgets\LinkPager::widget([
                                    'pagination' => $pages
                                ]) ?>
                            </div><!-- End .pull-right -->
                        </div><!-- End pagination-container -->


                    </div><!-- End .col-md-9 -->

                    <aside class="col-md-3 col-sm-4 col-xs-12 sidebar">
                        <div class="widget">
                            <div class="panel-group custom-accordion sm-accordion" id="category-filter">
                                <div class="panel">
                                    <div class="accordion-header">
                                        <div class="accordion-title"><span><?= Yii::t('app', 'Category') ?></span></div>
                                        <!-- End .accordion-title -->
                                        <a class="accordion-btn opened" data-toggle="collapse"
                                           data-target="#category-list-1"></a>
                                    </div><!-- End .accordion-header -->

                                    <div id="category-list-1" class="collapse in">
                                        <div class="panel-body">
                                            <?= \yii\helpers\Html::hiddenInput('GoodsSearch[type]', $curr_type) ?>
                                            <ul class="category-filter-list jscrollpane" id="type-ul">
                                                <?php foreach ($types as $type): ?>
                                                <li><a href="#" id="<?= $type->name ?>">
                                                        <?= $type->name ?> (<?= $type->count ?>)</a>
                                                    <?= $type->name == $curr_type ? '<i class="glyphicon glyphicon-ok"></i>' : ''?></li>
                                                <?php endforeach ?>
                                            </ul>
                                        </div><!-- End .panel-body -->
                                    </div><!-- #collapse -->
                                </div><!-- End .panel -->

                                <div class="panel">
                                    <div class="accordion-header">
                                        <div class="accordion-title"><span><?= Yii::t('app', 'Brand') ?></span></div>
                                        <!-- End .accordion-title -->
                                        <a class="accordion-btn opened" data-toggle="collapse"
                                           data-target="#category-list-2"></a>
                                    </div><!-- End .accordion-header -->

                                    <div id="category-list-2" class="collapse in">
                                        <div class="panel-body">
                                            <?= \yii\helpers\Html::hiddenInput('GoodsSearch[brand]', $curr_brand) ?>
                                            <ul class="category-filter-list jscrollpane" id="brand-ul">
                                                <?php foreach ($brands as $brand): ?>
                                                    <li><a href="#" id="<?= $brand->name ?>">
                                                            <?= $brand->name ?> (<?= $brand->count ?>)</a>
                                                        <?= $brand->name == $curr_brand ? '<i class="glyphicon glyphicon-ok"></i>' : ''?></li>
                                                <?php endforeach ?>
                                            </ul>
                                        </div><!-- End .panel-body -->
                                    </div><!-- #collapse -->
                                </div><!-- End .panel -->

                                <div class="panel">
                                    <div class="accordion-header">
                                        <div class="accordion-title"><span><?= Yii::t('app', 'Price') ?></span></div>
                                        <!-- End .accordion-title -->
                                        <a class="accordion-btn opened" data-toggle="collapse"
                                           data-target="#category-list-3"></a>
                                    </div><!-- End .accordion-header -->

                                    <div id="category-list-3" class="collapse in">
                                        <div class="panel-body">
                                            <div id="price-range">

                                            </div><!-- End #price-range -->
                                            <div id="price-range-details">
                                                <span class="sm-separator"><?= Yii::t('app', 'from') ?></span>
                                                <input type="text" id="price-range-low" class="separator" name="GoodsSearch[priceFrom]"
                                                value="<?= $searchModel->priceFrom ?>">
                                                <span class="sm-separator"><?= Yii::t('app', 'to') ?></span>
                                                <input type="text" id="price-range-high" name="GoodsSearch[priceTo]"
                                                value="<?= $searchModel->priceTo ?>">
                                            </div>
                                            <div id="price-range-btns">
                                                <span id="price-ok" class="btn btn-custom-2 btn-sm"><?= Yii::t('app', 'Ok') ?></span>
                                                <button id="price-clear" class="btn btn-custom-2 btn-sm"><?= Yii::t('app', 'Clear') ?></button>
                                            </div>

                                        </div><!-- End .panel-body -->
                                    </div><!-- #collapse -->
                                </div><!-- End .panel -->
                            </div><!-- .panel-group -->
                        </div><!-- End .widget -->

                    </aside><!-- End .col-md-3 -->
                </div><!-- End .row -->


            </div><!-- End .col-md-12 -->
        </div><!-- End .row -->
        <?php \yii\widgets\ActiveForm::end() ?>
    </div><!-- End .container -->
</div>

