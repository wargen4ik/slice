<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 11.06.18
 * Time: 22:57
 */
/* @var $model \common\models\Goods */
?>

<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="item">
        <div class="item-image-wrapper">
            <figure class="item-image-container">
                <a href="/product/<?= $model->id ?>">
                    <img src="<?= $model->logo->getUrl() ?>" class="item-image" style="height: 130px;">
                    <img src="<?= $model->logo->getUrl() ?>" class="item-image-hover" style="height: 130px;">
                </a>
            </figure>
        </div>
        <div class="item-meta-container">
            <div class="item-meta-inner-container clearfix">

                <div class="item-price-container inline pull-left">
                    <span class="item-price">₴<?= $model->price ?></span>
                </div>
                <div class="ratings-container pull-right">
                    <div class="ratings">
                        <div class="ratings-result" data-result="<?= $model->getRating() ?>"></div>
                    </div>
                </div>

            </div>

            <h3 class="item-name"><a href="/product/<?= $model->id ?>"><?= $model->name ?></a></h3>
            <div class="item-action">
                <a href="/bucket/add?id=<?= $model->id ?>&return_url=<?= Yii::$app->request->url ?>" class="item-add-btn">
                    <span class="icon-cart-text"><?= Yii::t('app', 'Add to Cart') ?></span>
                </a>
            </div>
        </div>
    </div>
</div>