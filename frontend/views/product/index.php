<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 07.06.18
 * Time: 21:33
 */

use common\models\Goods;

/* @var $goods \common\models\Goods */
/* @var $reviews \common\models\Review[] */

$this->registerJs(<<<JS
    $('#quantity').change(function() {
        if ($(this).val() <= 0){
            $(this).val(1);
        }
    })
JS
)
?>

<div class="row">
    <div class="col-xs-11">
        <div id="breadcrumb-container">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/"><?= Yii::t('app', 'Home') ?></a></li>
                    <li class="active"><?= Yii::t('app', 'Product') ?></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="row">

                    <div class="col-md-6 col-sm-12 col-xs-12 product-viewer clearfix">

                        <div id="product-image-carousel-container">
                            <ul id="product-carousel" class="celastislide-list">
                                <?php foreach ($goods->images as $image): ?>
                                    <li class="<?= $goods->logo->id == $image->id ? 'active-slide' : '' ?>"><a
                                                data-rel='prettyPhoto[product]' href="<?= $image->getUrl() ?>"
                                                data-image="<?= $image->getUrl() ?>"
                                                data-zoom-image="<?= $image->getUrl() ?>"
                                                class="product-gallery-item"><img src="<?= $image->getUrl() ?>"></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>

                        <div id="product-image-container">
                            <figure><img src="<?= $goods->logo->getUrl() ?>"
                                         data-zoom-image="<?= $goods->logo->getUrl() ?>" id="product-image">
                            </figure>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 product">
                        <div class="lg-margin visible-sm visible-xs"></div>
                        <h1 class="product-name"><?= $goods->name ?></h1>
                        <div class="ratings-container">
                            <div class="ratings separator">
                                <div class="ratings-result" data-result="<?= $goods->getRating() ?>"></div>
                            </div>
                            <span class="ratings-amount separator">
									<?= count($goods->reviews) ?>
								</span>
                        </div>
                        <ul class="product-list">
                            <li><span><?= Yii::t('app', 'Availability') ?>
                                    :</span><?= $goods->is_available ? Yii::t('app', 'In Stock') : Yii::t('app', 'Not available') ?>
                            </li>
                            <li><span><?= Yii::t('app', 'Product Code') ?>:</span><?= $goods->id ?></li>
                            <li><span><?= Yii::t('app', 'Brand') ?>:</span><?= $goods->brand->name ?></li>
                        </ul>
                        <hr>
                        <div class="product-add clearfix">
                            <div class="custom-quantity-input">
                                <input type="text" name="quantity" id="quantity" value="1">
                                <span
                                   onclick="$(this).parent().find('input').val(parseInt($(this).parent().find('input').val()) + 1);"
                                   class="quantity-btn quantity-input-up"><i class="fa fa-angle-up"></i></span>
                                <span
                                   onclick="var input = $(this).parent().find('input'); if (input.val() > 1){input.val(input.val() - 1);}"
                                   class="quantity-btn quantity-input-down"><i class="fa fa-angle-down"></i></span>
                            </div>
                            <button id="add-to-card"
                                    class="btn btn-custom-2"><?= Yii::t('app', 'ADD TO CART') ?></button>
                            <?php
                            $this->registerJs(<<<JS
                                $('#add-to-card').click(function() {
                                  var href = "/bucket/add?id={$goods->id}&return_url=/product/{$goods->id}&count=" + $(this).parent().find('input').val();
                                  window.location.replace(href);
                                });
JS
                            )
                            ?>
                        </div>
                        <div class="md-margin"></div>
                        <div class="product-extra clearfix">
                            <div class="product-extra-box-container clearfix">
                            </div>
                            <div class="md-margin visible-xs"></div>
                        </div>
                    </div>


                </div>

                <div class="lg-margin2x"></div>

                <div class="row">
                    <div class="col-md-9 col-sm-12 col-xs-12">

                        <div class="tab-container left product-detail-tab clearfix">
                            <ul class="nav-tabs">
                                <li class="active"><a href="#overview"
                                                      data-toggle="tab"><?= Yii::t('app', 'Overview') ?></a>
                                </li>
                            </ul>
                            <div class="tab-content clearfix">

                                <div class="tab-pane active" id="overview">
                                    <?= $goods->description ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="comments" style="margin-top: 60px">
                    <header class="title-bg">
                        <h3><?= Yii::t('app', 'COMMENTS ({count})', ['count' => count($reviews)]) ?></h3>
                    </header>
                    <ul class="comments-list">
                        <?php foreach ($reviews as $review): ?>
                        <li>
                            <div class="comment clearfix">
                                <div class="comment-details">
                                    <div class="comment-meta-container">
                                        <a href="#"><?= $review->user->email ?></a>
                                        <span><?= Yii::$app->formatter->asDatetime($review->created_at) ?></span>
                                    </div><!-- End .comment-meta-container -->
                                    <p><?= $review->text ?></p>
                                </div><!-- End .comment-details -->
                            </div><!-- End .comment -->
                        </li>
                        <?php endforeach; ?>
                    </ul>

                    <?php if ($can_add_review): ?>
                    <h4 class="sub-title"><?= Yii::t('app', 'Leave a reply') ?></h4>
                    <div class="row">
                        <?php $form = \yii\widgets\ActiveForm::begin(['action' => '/product/add-review?return_url=/product/'. $goods->id . '#comments', 'id' => 'comment-form']) ?>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class=" input-container normal-selectbox">
                                    <span class="input-group-addon"><span class="input-icon input-icon-message"></span><span class="input-text"><?= Yii::t('app', 'Your rating') ?>&#42;</span></span>
                                    <select name="rating" class="selectbox">
                                        <option selected value="1">1</option>
                                        <option selected value="2">2</option>
                                        <option selected value="3">3</option>
                                        <option selected value="4">4</option>
                                        <option selected value="5">5</option>
                                    </select>
                                </div><!-- End .input-group -->
                                <div class="input-group textarea-container">
                                    <span class="input-group-addon"><span class="input-icon input-icon-message"></span><span class="input-text"><?= Yii::t('app', 'Your Comment') ?>&#42;</span></span>
                                    <textarea name="text" id="comment-message" class="form-control" cols="30" rows="6" placeholder="Your Comment"></textarea>
                                </div><!-- End .input-group -->
                                <input type="hidden" name="user_id" value="<?= Yii::$app->user->id ?>">
                                <input type="hidden" name="goods_id" value="<?= $goods->id ?>">
                                <input type="submit" value="<?= Yii::t('app', 'POST COMMENT' )?>" class="btn btn-custom-2">
                            </div><!-- End .col-md-6 -->
                        <?php \yii\widgets\ActiveForm::end() ?>
                    </div><!-- End .row -->
                    <?php endif; ?>
                </div><!-- End .comments -->
                <div class="lg-margin2x"></div>
                <div class="purchased-items-container carousel-wrapper" id="also-purchased">
                    <header class="content-title">
                        <div class="title-bg">
                            <h2 class="title"><?= Yii::t('app', 'Also Purchased') ?></h2>
                        </div>
                    </header>

                    <div class="carousel-controls">
                        <div id="purchased-items-slider-prev" class="carousel-btn carousel-btn-prev"></div>
                        
                        <div id="purchased-items-slider-next"
                             class="carousel-btn carousel-btn-next carousel-space"></div>
                        
                    </div>
                    <div class="purchased-items-slider owl-carousel">
                        <?php foreach (Goods::getAlsoPurchased() as $alsoPurchased): ?>
                        <div class="item">
                            <div class="item-image-wrapper">
                                <figure class="item-image-container">
                                    <a href="/product/<?= $alsoPurchased->id ?>">
                                        <img src="<?= $alsoPurchased->logo->getUrl() ?>" class="item-image">
                                        <img src="<?= $alsoPurchased->logo->getUrl() ?>" class="item-image-hover">
                                    </a>
                                </figure>
                            </div>
                            <div class="item-meta-container">
                                <div class="item-meta-inner-container clearfix">

                                    <div class="item-price-container inline pull-left">
                                        <span class="item-price">₴<?= $alsoPurchased->price ?></span>
                                    </div>
                                    <div class="ratings-container pull-right">
                                        <div class="ratings">
                                            <div class="ratings-result" data-result="<?= $alsoPurchased->getRating() ?>"></div>
                                        </div>
                                    </div>

                                </div>

                                <h3 class="item-name"><a href="/product/<?= $alsoPurchased->id ?>"><?= $alsoPurchased->name ?></a></h3>
                                <div class="item-action">
                                    <a href="/bucket/add?id=<?= $alsoPurchased->id ?>&return_url=<?= Yii::$app->request->url ?>#also-purchased" class="item-add-btn">
                                        <span class="icon-cart-text"><?= Yii::t('app', 'Add to Cart') ?></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

