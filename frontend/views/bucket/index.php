<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 16.06.18
 * Time: 18:03
 */

use common\models\Goods;

/* @var $bucket_items \common\models\BucketItem[] */
/* @var $delivery \common\models\Delivery */

$add_url = \yii\helpers\Url::to(['/bucket/add']);
$remove_url = \yii\helpers\Url::to(['/bucket/remove']);

$this->registerJs(<<<JS

    function changePrice(block, input, change, resp) {
          var price_block = block.find('.item-total-col').find('.item-price-special'),
          prev = price_block.data('prev');
          price_block.data('prev', price_block.text().replace('₴', ''));
          price_block.text('₴' + resp['total_price']);
          input.val(parseInt(input.val()) + change);
          var total_price = $('#total-price');
          if (change > 0){
              total_price.text('₴' + (parseInt(total_price.text().replace('₴', '')) + resp['price']));
          } else {
              total_price.text('₴' + (parseInt(total_price.text().replace('₴', '')) - resp['price']));
          }
    }
    $('.count-up').click(function() {
        var me = $(this),
        input = me.parent().find('input');
        $.post('{$add_url}?id=' + me.data('id'), function(resp) {
          if (resp['success']){
              var block = me.parent().parent().parent();
              changePrice(block, input, 1, resp);
          }
        })
    });

    $('.count-down').click(function() {
        var me = $(this),
        input = me.parent().find('input');
        if (parseInt(input.val()) <= 1){
            return;
        }
        $.post('{$add_url}?id=' + me.data('id') + '&count=-1', function(resp) {
          if (resp['success']){
              var block = me.parent().parent().parent();
              changePrice(block, input, -1, resp);
          }
        })
    });
    $('.remove').click(function() {
        var me = $(this);
        $.post('{$remove_url}?id=' + me.data('id'), function(resp) {
          if (resp['success']){
              var total_price = $('#total-price');
              total_price.text('₴' + (parseInt(total_price.text().replace('₴', '')) - resp['total_price']));
              me.parent().parent().remove();
          }
          })
    });
JS
);
?>


<div class="row">
    <div class="col-xs-11">
        <div id="breadcrumb-container">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/"><?= Yii::t('app', 'Home') ?></a></li>
                    <li class="active"><?= Yii::t('app', 'Shopping Cart') ?></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <header class="content-title">
                    <h1 class="title"><?= Yii::t('app', 'Shopping Cart') ?></h1>
                </header>
                <div class="xs-margin"></div><!-- space -->
                <div class="row">

                    <div class="col-md-12 table-responsive">

                        <table class="table cart-table">
                            <thead>
                            <tr>
                                <th class="table-title"><?= Yii::t('app', 'Product Name') ?></th>
                                <th class="table-title"><?= Yii::t('app', 'Product Code') ?></th>
                                <th class="table-title"><?= Yii::t('app', 'Price') ?></th>
                                <th class="table-title"><?= Yii::t('app', 'Quantity') ?></th>
                                <th class="table-title"><?= Yii::t('app', 'SubTotal') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $total_price = 0 ?>
                            <?php foreach ($bucket_items as $bucket_item): ?>
                                <tr>
                                    <td class="item-name-col">
                                        <figure>
                                            <a href="/product/<?= $bucket_item->goods->id ?>"><img
                                                        src="<?= $bucket_item->goods->logo->url ?>"></a>
                                        </figure>
                                        <header class="item-name"><a
                                                    href="/product/<?= $bucket_item->goods->id ?>"><?= $bucket_item->goods->name ?></a>
                                        </header>
                                        <ul>
                                            <li><?= Yii::t('app', 'Type') ?>
                                                : <?= $bucket_item->goods->type->name ?></li>
                                            <li><?= Yii::t('app', 'Brand') ?>
                                                : <?= $bucket_item->goods->brand->name ?></li>
                                        </ul>
                                    </td>
                                    <td class="item-code"><?= $bucket_item->goods->id ?></td>
                                    <td class="item-price-col"><span
                                                class="item-price-special">₴<?= $bucket_item->goods->price ?></span>
                                    </td>
                                    <td>
                                        <div class="custom-quantity-input">
                                            <input disabled type="text" name="quantity" data-id="<?= $bucket_item->goods_id ?>"
                                                   value="<?= $bucket_item->count ?>">
                                            <span data-id="<?= $bucket_item->goods_id ?>"
                                                  class="quantity-btn quantity-input-up count-up"><i class="fa fa-angle-up"></i></span>
                                            <span data-id="<?= $bucket_item->goods_id ?>"
                                                  class="quantity-btn quantity-input-down count-down"><i
                                                        class="fa fa-angle-down"></i></span>
                                        </div>
                                    </td>
                                    <td class="item-total-col"><span class="item-price-special"
                                                                     data-prev="<?= $bucket_item->goods->price * $bucket_item->count ?>">₴<?= $bucket_item->goods->price * $bucket_item->count ?></span>
                                        <button data-id="<?= $bucket_item->id ?>" class="close-button remove"></button>
                                    </td>
                                </tr>
                                <?php $total_price += $bucket_item->count * $bucket_item->goods->price ?>
                            <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div><!-- End .col-md-12 -->

                </div><!-- End .row -->
                <div class="lg-margin"></div><!-- End .space -->

                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12 lg-margin">

                        <div class="tab-container left clearfix">
                            <ul class="nav-tabs">
                                <li class="active"><a href="#shipping"
                                                      data-toggle="tab"><?= Yii::t('app', 'Delivery'); ?></a></li>

                            </ul>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="shipping">

                                    <?php $form = \yii\widgets\ActiveForm::begin() ?>
                                    <p class="shipping-desc"><?= Yii::t('app', 'Enter your delivery info.') ?></p>
                                    <div class="form-group">
                                        <label class="control-label"><?= Yii::t('app', 'Delivery type') ?>&#42;</label>
                                        <div class="input-container normal-selectbox">
                                            <?= \yii\helpers\Html::activeDropDownList($delivery, 'delivery_type', \common\models\Delivery::getTypes(), ['class' => 'selectbox', 'label' => false]); ?>
                                        </div><!-- End .select-container -->
                                    </div><!-- End .form-group -->
                                    <div class="xss-margin"></div>
                                    <div class="form-group">
                                        <label class="control-label"><?= Yii::t('app', 'Your name') ?>&#42;</label>
                                        <div class="input-container normal-selectbox">
                                            <?= $form->field($delivery, 'fio')->label(false); ?>
                                        </div><!-- End .select-container -->
                                    </div><!-- End .form-group -->
                                    <div class="xss-margin"></div>
                                    <div class="form-group">
                                        <label for="select-country"
                                               class="control-label"><?= Yii::t('app', 'Address') ?>&#42;</label>
                                        <div class="input-container">
                                            <?= $form->field($delivery, 'address')->label(false); ?>
                                        </div>
                                    </div><!-- End .form-group -->
                                    <div class="xss-margin"></div>
                                    <div class="form-group">
                                        <label for="select-country"
                                               class="control-label"><?= Yii::t('app', 'Phone number') ?>&#42;</label>
                                        <div class="input-container">
                                            <?= $form->field($delivery, 'phone')->label(false); ?>
                                        </div>
                                    </div><!-- End .form-group -->
                                    <div class="xss-margin"></div>
                                    <p class="text-right">
                                        <input type="submit" class="btn btn-custom-2"
                                               value="<?= Yii::t('app', 'Checkout') ?>">
                                    </p>
                                    <?php \yii\widgets\ActiveForm::end() ?>

                                </div><!-- End .tab-pane -->
                            </div><!-- End .tab-content -->
                        </div><!-- End .tab-container -->

                    </div><!-- End .col-md-8 -->
                    <div class="col-md-4 col-sm-12 col-xs-12">

                        <table class="table total-table">
                            <tfoot>
                            <tr>
                                <td><?= Yii::t('app', 'Total') ?>:</td>
                                <td id="total-price">₴<?= $total_price ?></td>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="md-margin"></div><!-- End .space -->
                        <a href="/product/shop" class="btn btn-custom-2"><?= Yii::t('app', 'CONTINUE SHOPPING') ?></a>
                    </div><!-- End .col-md-4 -->
                </div><!-- End .row -->
                <div class="md-margin2x"></div><!-- Space -->

                <div class="purchased-items-container carousel-wrapper" id="also-purchased">
                    <header class="content-title">
                        <div class="title-bg">
                            <h2 class="title"><?= Yii::t('app', 'Also Purchased') ?></h2>
                        </div>
                    </header>

                    <div class="carousel-controls">
                        <div id="purchased-items-slider-prev" class="carousel-btn carousel-btn-prev"></div>

                        <div id="purchased-items-slider-next"
                             class="carousel-btn carousel-btn-next carousel-space"></div>

                    </div>
                    <div class="purchased-items-slider owl-carousel">
                        <?php foreach (Goods::getAlsoPurchased() as $alsoPurchased): ?>
                            <div class="item">
                                <div class="item-image-wrapper">
                                    <figure class="item-image-container">
                                        <a href="/product/<?= $alsoPurchased->id ?>">
                                            <img src="<?= $alsoPurchased->logo->getUrl() ?>" class="item-image">
                                            <img src="<?= $alsoPurchased->logo->getUrl() ?>" class="item-image-hover">
                                        </a>
                                    </figure>
                                </div>
                                <div class="item-meta-container">
                                    <div class="item-meta-inner-container clearfix">

                                        <div class="item-price-container inline pull-left">
                                            <span class="item-price">₴<?= $alsoPurchased->price ?></span>
                                        </div>
                                        <div class="ratings-container pull-right">
                                            <div class="ratings">
                                                <div class="ratings-result"
                                                     data-result="<?= $alsoPurchased->getRating() ?>"></div>
                                            </div>
                                        </div>

                                    </div>

                                    <h3 class="item-name"><a
                                                href="/product/<?= $alsoPurchased->id ?>"><?= $alsoPurchased->name ?></a>
                                    </h3>
                                    <div class="item-action">
                                        <a href="/bucket/add?id=<?= $alsoPurchased->id ?>&return_url=<?= Yii::$app->request->url ?>#also-purchased"
                                           class="item-add-btn">
                                            <span class="icon-cart-text"><?= Yii::t('app', 'Add to Cart') ?></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

            </div><!-- End .col-md-12 -->
        </div><!-- End .row -->
    </div>
</div>
