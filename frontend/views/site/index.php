<?php

/* @var $this yii\web\View */
/* @var $goods_all \common\models\Goods[] */

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

$this->title = 'My Yii Application';
?>


<div class="row">

    <div class="col-xs-11 main-content">
        <header class="content-title">
            <h2 class="title"><?= Yii::t('app', 'Our Products') ?> </h2>
            <p class="title-desc"><?= Yii::t('app', 'Save your money and time with our store. Here\'s the best part of our impressiveassortment.') ?></p>
        </header>
        <?php
        $menuItems = [];

        $menuItems[] = '<li class="active">'
            . Html::a(Yii::t('app', 'All'), ['/'], ['data-toggle' => 'tab'])
            . '</li>';
        $menuItems[] = '<li>'
            . Html::a(Yii::t('app', 'Latest'), ['/latest'], ['data-toggle' => 'tab'])
            . '</li>';
        $menuItems[] = '<li>'
            . Html::a(Yii::t('app', 'Bestsellers'), ['/bestsellers'], ['data-toggle' => 'tab'])
            . '</li>';
        echo Nav::widget([
            'options' => ['class' => 'tab-style-1 clearfix', 'id' => 'products-tabs-list'],
            'items' => $menuItems,
        ]);
        ?>

        <div id="products-tabs-content" class="row tab-content">
            <div class="tab-pane active" id="all">
                <?php foreach ($goods_all as $goods): ?>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <div class="item-image-wrapper">
                            <figure class="item-image-container">
                                <a href="/product/<?= $goods->id ?>">
                                    <img src="<?= $goods->logo->url ?>" alt="logo" class="item-image" style="height: 130px;">
                                    <img src="<?= $goods->logo->url ?>" alt="item1  Hover" class="item-image-hover" style="height: 130px;">
                                </a>
                            </figure><!-- End .item-image-container -->
                            <?php if ($goods->isNew()): ?>
                            <span class="new-circle top-left"><?= Yii::t('app', 'New') ?></span>
                            <?php endif; ?>

                        </div><!-- End .item-image-wrapper -->
                        <div class="item-meta-container">
                            <div class="item-meta-inner-container clearfix">

                                <div class="item-price-container inline pull-left">
                                    <span class="item-price">₴<?= $goods->price ?></span>
                                </div><!-- End .item-price-container -->
                                <div class="ratings-container pull-right">
                                    <div class="ratings">
                                        <div class="ratings-result" data-result="<?= $goods->getRating() ?>"></div>
                                    </div><!-- End .ratings -->
                                </div><!-- End .rating-container -->

                            </div><!-- End .item-meta-inner-container -->

                            <h3 class="item-name"><a href="/product/<?= $goods->id ?>"><?= $goods->name ?></a></h3>
                            <div class="item-action">
                                <a href="/bucket/add?id=<?= $goods->id ?>&return_url=/" class="item-add-btn">
                                    <span class="icon-cart-text"><?= Yii::t('app', 'Add to Cart') ?></span>
                                </a>
                            </div><!-- End .item-action -->
                        </div><!-- End .item-meta-container -->
                    </div><!-- End .item -->
                </div><!-- End .col-md-4 -->
                <?php endforeach; ?>
            </div><!-- End .tab-pane -->
        </div><!-- End #products-tabs-content -->

        <div class="xlg-margin"></div><!-- Space -->

        <div class="hot-items carousel-wrapper">
            <header class="content-title">
                <div class="title-bg">
                    <h2 class="title">On Sale</h2>
                </div><!-- End .title-bg -->
                <p class="title-desc">Only with us you can get a new model with a discount.</p>
            </header>

            <div class="carousel-controls">
                <div id="hot-items-slider-prev" class="carousel-btn carousel-btn-prev">
                </div><!-- End .carousel-prev -->
                <div id="hot-items-slider-next" class="carousel-btn carousel-btn-next carousel-space">
                </div><!-- End .carousel-next -->
            </div><!-- End .carousel-controls -->
            <div class="hot-items-slider owl-carousel">
                <?php foreach (\common\models\Goods::getHotGoods() as $alsoPurchased): ?>
                    <div class="item">
                        <div class="item-image-wrapper">
                            <figure class="item-image-container">
                                <a href="/product/<?= $alsoPurchased->id ?>">
                                    <img src="<?= $alsoPurchased->logo->getUrl() ?>" class="item-image">
                                    <img src="<?= $alsoPurchased->logo->getUrl() ?>" class="item-image-hover">
                                </a>
                            </figure>
                        </div>
                        <div class="item-meta-container">
                            <div class="item-meta-inner-container clearfix">

                                <div class="item-price-container inline pull-left">
                                    <span class="item-price">₴<?= $alsoPurchased->price ?></span>
                                </div>
                                <div class="ratings-container pull-right">
                                    <div class="ratings">
                                        <div class="ratings-result" data-result="<?= $alsoPurchased->getRating() ?>"></div>
                                    </div>
                                </div>

                            </div>

                            <h3 class="item-name"><a href="/product/<?= $alsoPurchased->id ?>"><?= $alsoPurchased->name ?></a></h3>
                            <div class="item-action">
                                <a href="/bucket/add?id=<?= $alsoPurchased->id ?>&return_url=<?= Yii::$app->request->url ?>#also-purchased" class="item-add-btn">
                                    <span class="icon-cart-text"><?= Yii::t('app', 'Add to Cart') ?></span>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div><!--hot-items-slider -->

            <div class="lg-margin"></div><!-- Space -->
        </div><!-- End .hot-items -->
    </div><!-- End .col-md-9 -->
</div><!-- End .row -->
