<?php

namespace common\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property int $goods_id
 * @property int $is_logo
 * @property string $path
 * @property string $name
 *
 * @property Goods $goods
 * @property string $url
 */
class Image extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['goods_id'], 'required'],
            [['goods_id', 'is_logo'], 'integer'],
            [['path'], 'string', 'max' => 511],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'goods_id' => 'Goods ID',
            'is_logo' => 'Is Logo',
            'path' => 'Path',
        ];
    }

    public function getGoods(){
        return $this->hasOne(Goods::className(), ['id' => 'goods_id']);
    }

    public static function getBasePath(){
        if (YII_DEBUG) {
            return FileHelper::normalizePath(Yii::getAlias('@frontend/web/uploads/'));
        } else {
            return FileHelper::normalizePath(dirname(__DIR__, 4) . '/www/slice/frontend/web/uploads/');
        }
    }

    public function getFullPath(){
        if ($this->path)
            return $this->path . '/' . $this->name;
        return null;
    }

    public function getUrl(){
        if ($this->name)
            return Yii::getAlias('@site') . "/image?name=$this->name";
        return null;
    }

    public function delete(){
        try {
            unlink($this->getFullPath());
        } catch (\Exception $ex) {
        }
        return parent::delete();
    }

    public function uploadLogo($goods_id){
        if (!$this->isNewRecord){
            try {
                unlink($this->getFullPath());
            } catch (\Exception $ex) {
            }
        }
        $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
        if (!$this->imageFile){
            $this->addError('imageFile', 'You need to upload logo');
            return false;
        }
        if ($this->scenario !== 'create' && !$this->imageFile) {
            return true;
        }
        $timestamp = (new \DateTime())->getTimestamp();
        $this->name = $timestamp . $this->imageFile->baseName . '.' . $this->imageFile->extension;
        $this->path = static::getBasePath();
        $this->is_logo = true;
        $this->goods_id = $goods_id;
        if ($this->save()) {
            $this->imageFile->saveAs($this->getFullPath());
            return true;
        } else {
            return false;
        }
    }
}
