<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bucket_item".
 *
 * @property int $id
 * @property int $user_id
 * @property string $session_id
 * @property int $goods_id
 * @property int $count
 * @property boolean $is_active
 *
 * @property User $user
 * @property Goods $goods
 */
class BucketItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bucket_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'goods_id', 'count'], 'integer'],
            [['goods_id', 'count'], 'required'],
            [['session_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'session_id' => Yii::t('app', 'Session ID'),
            'goods_id' => Yii::t('app', 'Goods ID'),
        ];
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getGoods(){
        return $this->hasOne(Goods::className(), ['id' => 'goods_id']);
    }
}
