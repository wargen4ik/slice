<?php

namespace common\models;

use backend\models\BrandSearch;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "goods".
 *
 * @property int $id
 * @property string $name
 * @property int $price
 * @property string $description
 * @property int $is_available
 * @property int $type_id
 * @property int $brand_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Image[] $images
 * @property Image $logo
 * @property Type $type
 * @property Brand $brand
 * @property Review[] $reviews
 */
class Goods extends \yii\db\ActiveRecord
{

    const NEW_TIMESTAMP = 86400; // 3 days
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goods';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price', 'type_id', 'brand_id'], 'required'],
            [['price', 'is_available', 'type_id', 'brand_id', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'description' => 'Description',
            'is_available' => 'Is Available',
            'type_id' => 'Type ID',
            'brand_id' => 'Brand ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {

        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        if ($this->logo){
            $this->logo->delete();
        }
        if ($this->images) {
            foreach ($this->images as $image) {
                $image->delete();
            }
        }
        return parent::beforeDelete();
    }

    public function getType(){
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    public function getBrand(){
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    public function getImages(){
        return $this->hasMany(Image::className(), ['goods_id' => 'id']);
    }

    public function getLogo(){
        return $this->hasOne(Image::className(), ['goods_id' => 'id'])->onCondition(['is_logo' => 1]);
    }

    public function getReviews(){
        return $this->hasMany(Review::className(), ['goods_id' => 'id']);
    }

    public function isNew(){
        return ($this->created_at - time()) < self::NEW_TIMESTAMP;
    }

    public function getRating(){
        return Review::findBySql('select (sum(rating)/count(`review`.`id`)) sum from `review` left join `goods` on `goods`.`id` = `goods_id` where `goods`.`id`='. $this->id)->one()->sum * 20;
    }

    /* @return Goods[] */
    public function getRelatedGoods($limit = 5){
        return static::find()
//            ->where(['!=', 'id', $this->id])
            ->orderBy(new Expression('RAND()'))
            ->limit($limit)
            ->all();
    }

    /* @return Goods[] */
    public static function getAlsoPurchased($limit = 10){
        return static::find()
//            ->where(['!=', 'id', $this->id])
            ->orderBy(new Expression('RAND()'))
            ->limit($limit)
            ->all();
    }

    /* @return Goods[] */
    public static function getHotGoods($limit = 10){
        return static::find()
//            ->where(['!=', 'id', $this->id])
            ->orderBy(new Expression('RAND()'))
            ->limit($limit)
            ->all();
    }
}
