<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $user_id
 * @property string $session_id
 * @property int $created_at
 * @property int $delivery_id
 *
 * @property User $user
 * @property Delivery $delivery
 * @property BucketItem[] $bucketItems
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'delivery_id'], 'integer'],
            [['created_at', 'delivery_id'], 'required'],
            [['session_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'session_id' => Yii::t('app', 'Session ID'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getDelivery(){
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    public function getBucketItems(){
        return $this->hasMany(OrderBucketItem::className(), ['order_id' => 'id']);
    }
}
