<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_bucket_item".
 *
 * @property int $id
 * @property int $order_id
 * @property int $bucket_item_id
 */
class OrderBucketItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_bucket_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'bucket_item_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'bucket_item_id' => Yii::t('app', 'Bucket Item ID'),
        ];
    }

    public function getBucketItem(){
        return $this->hasOne(BucketItem::className(), ['id' => 'bucket_item_id']);
    }
}
